Idea Execution Plan

First stage:

 Creation of a simple CSS Framework that would follow GNOME Design guidelines and other guidelines/standards made by Caroline and maybe the GNOME Design Team. This little framework would have the foundation set of generic attributes, tags and selectors (Buttons, Grid, Containers, Colours, etc) that could be used for anyone interested in it.
 Evaluate the current content existing inside GNOME.org. And create an inventory of which pages, categories, and sections should be created.
 Should there be any new additional pages? Which content-wise changes should be made.
 Start to evaluate design and UI schematics of how the website should look and feel in order to place the content in the best possible way.
 What containers, sections and how the website should be wrapped? The creation of skeletons could help with that.
 Decide on which Content Management Platform or Framework we would like to adopt for the new GNOME.org page that would fit best for Designers, Content Editors and be easy, open-source, support content internationalization (i18n) 
and [... Insert other requirements here].


 Evaluation of requirements should be done
 Some of the options are 
https://forestry.io/, 
https://jekyllrb.com/,
 https://www.netlifycms.org/.
 https://ghost.org/, 
https://gohugo.io/, and 
https://wordpress.org




The next steps would be:

 Creating a placeholder on GitLab
 Organize the Repository (Milestones, Labels, etc)
 Start to evaluate the current content that we have and how we want to organize it or which strategies we want to adopt for the content placement
 What are the pages, categories, sections that the new website should have?
 Start to evaluate how the content should be structured.
 How should be the landing pages, which one are the most important ones?
 Eg.: Download Pages
 How would be the sections of the page? What are the macro components? (Header, Footer, etc.)
 Elencate Design-Wide Issues
 What are the components that should be created inside the Framework that could support our content
 Which selectors, tags, etc we want to create. And which kind of components we want to create?
 Start to create the Assets-Repository related Issues
 Enter the Technical Requirements and Issues, like Languages, Folder Structure, etc.
 Start to wrangle the content and see UI schematics/samples/concepts
 Start the development of the Framework
 Start the creation of the Skeletons
 Start the integration with the chosen Content Management Framework/CMS
 Start the content placement using the Framework
Good Luck!


Reference: Discussion from Chatroom.
Credits - Claudio Wunder. 

